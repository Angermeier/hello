package org.tangerme.quarkus.hello.services;

import org.tangerme.quarkus.hello.model.SpyEntries;
import org.tangerme.quarkus.hello.model.SpyEntry;

import javax.enterprise.context.SessionScoped;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@SessionScoped
public class NSAService {

    private final Map<String, AtomicLong> content = new HashMap<>();

    public synchronized void registerRequest(String url) {
        if (!content.containsKey(url)) {
            content.put(url, new AtomicLong(0l));
        }
        content.get(url).incrementAndGet();
    }

    public SpyEntries getWhatsStored() {
        SpyEntries spyEntries = new SpyEntries();
        spyEntries.setSpyEntries(content.entrySet().stream()
                .map(x->new SpyEntry(x.getKey(),x.getValue().get()))
                .collect(Collectors.toList()));
        return spyEntries;
    }
}
