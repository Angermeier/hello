package org.tangerme.quarkus.hello.controllers;

import org.tangerme.quarkus.hello.services.NSAService;
import org.tangerme.quarkus.hello.model.SpyEntries;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/loophole")
public class NSAResource {

    @Inject
    NSAService nsaService;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN})
    public SpyEntries spy() {
        return nsaService.getWhatsStored();
    }
}
