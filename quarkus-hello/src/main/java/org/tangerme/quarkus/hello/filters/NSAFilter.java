package org.tangerme.quarkus.hello.filters;

import org.tangerme.quarkus.hello.services.NSAService;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

@Provider
public class NSAFilter implements ContainerRequestFilter {

    @Context
    UriInfo info;

    @Inject
    NSAService nsaService;

    @Override
    public void filter(ContainerRequestContext context) {
        nsaService.registerRequest(info.getPath());
    }
}