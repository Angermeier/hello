package org.tangerme.quarkus.hello.model;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.stream.Collectors;

@XmlRootElement(name = "spyentries")
@XmlAccessorType(XmlAccessType.FIELD)
public class SpyEntries {
    @XmlElement(name = "spyentry")
    @Getter
    @Setter
    private List<SpyEntry> spyEntries;

    @Override
    public String toString() {
        return getSpyEntries().stream().map(x-> x.toString()).collect(Collectors.joining("\n"));
    }
}
