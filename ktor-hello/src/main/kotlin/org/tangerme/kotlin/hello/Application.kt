package org.tangerme.kotlin.hello

import io.ktor.application.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import kotlinx.coroutines.flow.*
import java.util.*

fun main(args: Array<String>) {
    embeddedServer(Netty, port = 8080) { module() }.start(wait = true)
}

fun pathFlow(s: String) = flow {
    emit(s)
}

fun Application.module() {
    val paths = LinkedList<String>()
    intercept(ApplicationCallPipeline.Features) {
        paths.add(call.request.uri)
    }
    routing {
        get("/") {
            call.respondText("Hello World!")
        }
        get("/hello/{name}") {
            call.respondText("Hello ${call.parameters["name"]}")
        }
        get("/hello") {
            call.respondText("Hello worldy!")
        }
        get("/nsa") {
            call.respondText(paths.toSet().asFlow().map { path -> path + ": " + paths.count { path == it } }.reduce { a, b -> a + "\n" + b })
        }
    }
}