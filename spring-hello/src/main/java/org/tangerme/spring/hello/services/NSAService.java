package org.tangerme.spring.hello.services;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import org.tangerme.spring.hello.model.SpyEntries;
import org.tangerme.spring.hello.model.SpyEntry;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Component
@SessionScope
public class NSAService {

    private final Map<String, AtomicLong> content = new HashMap<>();

    public synchronized void registerRequest(String url) {
        if (!content.containsKey(url)) {
            content.put(url, new AtomicLong(0l));
        }
        content.get(url).incrementAndGet();
    }

    public SpyEntries getWhatsStored() {
        SpyEntries spyEntries = new SpyEntries();
        spyEntries.setSpyEntries(content.entrySet().stream()
                .map(x->new SpyEntry(x.getKey(),x.getValue().get()))
                .collect(Collectors.toList()));
        return spyEntries;
    }
}
