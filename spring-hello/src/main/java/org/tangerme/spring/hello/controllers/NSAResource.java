package org.tangerme.spring.hello.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.tangerme.spring.hello.model.SpyEntries;
import org.tangerme.spring.hello.services.NSAService;

@RestController
public class NSAResource {

    @Autowired
    NSAService nsaService;

    @GetMapping("/loophole")
    public ResponseEntity<SpyEntries> hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new ResponseEntity<>(nsaService.getWhatsStored(), HttpStatus.OK);
    }
}
