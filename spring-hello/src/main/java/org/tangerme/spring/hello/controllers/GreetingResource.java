package org.tangerme.spring.hello.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.tangerme.spring.hello.services.GreetingService;

@RestController
public class GreetingResource {

    @Autowired
    GreetingService greetingService;

    @GetMapping("/hello/{name}")
    public String hello(@PathVariable(value = "name") String name) {
        return greetingService.greet(name);
    }

    @GetMapping("/hello")
    public String hello() {
        return "Hello Worldy!";
    }
}
