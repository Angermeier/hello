package org.tangerme.spring.hello.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class SpyEntry {

    @Getter
    @Setter
    @XmlElement
    @JsonProperty
    private String url;

    @Getter
    @Setter
    @XmlElement
    @JsonProperty
    private long count;

    public SpyEntry(String url, long count) {
        setCount(count);
        setUrl(url);
    }

    public String toString() {
        return String.format("\"%s\": %s", getUrl(), getCount());
    }

}
