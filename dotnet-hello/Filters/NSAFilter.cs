using System;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace dotnet_hello.Filters
{
    public class NSAFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            String path = context.HttpContext.Request.Path;
            ISession session = context.HttpContext.Session;
            Dictionary<String, int> dict = session.Get<Dictionary<String, int>>("nsa");
            dict = dict == null ? new Dictionary<String, int>() : dict;
            if(!dict.ContainsKey(path)) {
                dict[path] = 0;
            }
            dict[path] = dict[path] + 1;
            session.Set<Dictionary<String, int>>("nsa", dict);
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}
