﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using dotnet_hello.Filters;

namespace dotnet_hello.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HelloController : ControllerBase
    {

        private readonly ILogger<HelloController> _logger;

        public HelloController(ILogger<HelloController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [Route("/hello/{name}")]
        [ServiceFilter(typeof(NSAFilter))]
        public String Get(String name = "C# World")
        {
            return "Hello " + name + "!";
        }

        [HttpGet]
        [Route("/hello")]
        [ServiceFilter(typeof(NSAFilter))]
        public String Get()
        {
            return "Hello C# World!";
        }
    }
}
