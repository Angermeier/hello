using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using dotnet_hello.Filters;

namespace dotnet_hello.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NSAController : ControllerBase
    {
        private readonly ILogger<NSAController> _logger;

        public NSAController(ILogger<NSAController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [Route("/loophole")]
        [ServiceFilter(typeof(NSAFilter))]
        public OkObjectResult Get()
        {
            return Ok(HttpContext.Session.Get<Dictionary<String, int>>("nsa"));
        }
    }
}
